package com.ufro.Dees.models;
import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;

import java.util.HashSet;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="imagen")
public class Imagen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="imagen_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    @JsonProperty(access = Access.WRITE_ONLY)
    private Usuario usuario;

    @OneToMany(mappedBy = "imagen", cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Comentario> comentarios= new HashSet<>();

    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "imagenes_etiquetas", joinColumns = @JoinColumn(name = "imagen_id",
            referencedColumnName = "imagen_id"),
            inverseJoinColumns = @JoinColumn(name = "etiqueta_id", referencedColumnName = "etiqueta_id"))
    private Set<Etiqueta> etiquetas =new HashSet<>();

    @OneToOne
    @JsonIgnore
    @JoinColumn(name="deespoint_id")
    private DeesPoint deesPoint;

    public DeesPoint getDeesPoint() {
        return deesPoint;
    }

    public void setDeesPoint(DeesPoint deesPoint) {
        this.deesPoint = deesPoint;
    }

    private String urlImagen;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Set<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(Set<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public Set<Etiqueta> getEtiquetas() {
        return etiquetas;
    }

    public void setEtiquetas(Set<Etiqueta> etiquetas) {
        this.etiquetas = etiquetas;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public Imagen(){
        super();
    }
}
