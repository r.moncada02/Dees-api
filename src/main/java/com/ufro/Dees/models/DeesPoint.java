package com.ufro.Dees.models;

import javax.persistence.*;

@Entity
@Table(name="deespoint")
public class DeesPoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="dp_primer_nivel")
    private int primerNivel;
    @Column(name="dp_segundo_nivel")
    private int segundoNivel;
    @Column(name="dp_tercer_nivel")
    private int tercerNivel;
    @Column(name="dp_cuarto_nivel")
    private int cuartoNivel;
    @Column(name="dp_quinto_nivel")
    private int quintoNivel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPrimerNivel() {
        return primerNivel;
    }

    public void setPrimerNivel(int primerNivel) {
        this.primerNivel = primerNivel;
    }

    public int getSegundoNivel() {
        return segundoNivel;
    }

    public void setSegundoNivel(int segundoNivel) {
        this.segundoNivel = segundoNivel;
    }

    public int getTercerNivel() {
        return tercerNivel;
    }

    public void setTercerNivel(int tercerNivel) {
        this.tercerNivel = tercerNivel;
    }

    public int getCuartoNivel() {
        return cuartoNivel;
    }

    public void setCuartoNivel(int cuartoNivel) {
        this.cuartoNivel = cuartoNivel;
    }

    public int getQuintoNivel() {
        return quintoNivel;
    }

    public void setQuintoNivel(int quintoNivel) {
        this.quintoNivel = quintoNivel;
    }
}
