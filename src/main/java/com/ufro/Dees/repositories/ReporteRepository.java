package com.ufro.Dees.repositories;

import com.ufro.Dees.models.Reporte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReporteRepository extends JpaRepository<Reporte, Long> {
}
