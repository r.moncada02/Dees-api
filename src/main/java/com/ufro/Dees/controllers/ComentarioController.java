package com.ufro.Dees.controllers;

import com.ufro.Dees.models.Comentario;
import com.ufro.Dees.models.Imagen;
import com.ufro.Dees.models.Usuario;
import com.ufro.Dees.repositories.ComentarioRepository;
import com.ufro.Dees.repositories.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/comentario")
public class ComentarioController {

    @Autowired
    private ImagenRepository imagenRepository;
    @Autowired
    private ComentarioRepository comentarioRepository;

    @PostMapping
    public ResponseEntity<Comentario> guardarComentario(@Valid @RequestBody Comentario comentario) {
        Optional<Imagen> imagenOptional = imagenRepository.findById(comentario.getImagen().getId());
        if(!imagenOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        comentario.setImagen(imagenOptional.get());
        Comentario comentarioGuardado =comentarioRepository.save(comentario);
        URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(comentarioGuardado.getId()).toUri();
        return ResponseEntity.created(ubicacion).body(comentarioGuardado);
    }
}
