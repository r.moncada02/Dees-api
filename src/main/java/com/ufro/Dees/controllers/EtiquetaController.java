package com.ufro.Dees.controllers;

import com.ufro.Dees.models.Etiqueta;
import com.ufro.Dees.repositories.EtiquetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.CollationElementIterator;
import java.util.Collection;

@RestController
@RequestMapping("/api/etiqueta")
public class EtiquetaController {

    @Autowired
    private EtiquetaRepository etiquetaRepository;

    @GetMapping
    public ResponseEntity<Collection<Etiqueta>> listarEtiquetas(){
        return new ResponseEntity<>(etiquetaRepository.findAll(),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> guardarEtiqueta(@RequestBody Etiqueta etiqueta){
        return new ResponseEntity<>(etiquetaRepository.save(etiqueta),HttpStatus.OK);
    }
}
