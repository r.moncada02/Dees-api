package com.ufro.Dees.controllers;

import com.ufro.Dees.excepciones.ResourceNotFoundException;
import com.ufro.Dees.models.Etiqueta;
import com.ufro.Dees.models.Imagen;
import com.ufro.Dees.models.Usuario;
import com.ufro.Dees.repositories.ImagenRepository;
import com.ufro.Dees.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/imagen")
public class ImagenController {
    @Autowired
    private ImagenRepository imagenRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    @GetMapping
    public ResponseEntity<Page<Imagen>> listarImagenes(Pageable pageable){
        return ResponseEntity.ok(imagenRepository.findAll(pageable));
    }

    @PostMapping
    public ResponseEntity<Imagen> guardarImagen(@Valid @RequestBody Imagen imagen) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(imagen.getUsuario().getId());
        if(!usuarioOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        imagen.setUsuario(usuarioOptional.get());
        Imagen imagenGuardada =imagenRepository.save(imagen);
        URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(imagenGuardada.getId()).toUri();
        return ResponseEntity.created(ubicacion).body(imagenGuardada);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Imagen> actualizarImagen(@Valid @PathVariable Long id, @Valid @RequestBody Imagen imagen) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(imagen.getUsuario().getId());
        if(!usuarioOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        Optional<Imagen> imagenOptional = imagenRepository.findById(id);
        if(!imagenOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        imagen.setUsuario(usuarioOptional.get());
        imagen.setId(imagenOptional.get().getId());
        imagenRepository.save(imagen);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Imagen> eliminarImagen(@PathVariable Long id) {
        Optional<Imagen> imagenOptional = imagenRepository.findById(id);
        if (!imagenOptional.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        imagenRepository.delete(imagenOptional.get());
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Imagen> obtenerImagen(@PathVariable Long id) {
        Optional<Imagen> imagenOptional = imagenRepository.findById(id);
        if (!imagenOptional.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(imagenOptional.get());
    }

    @GetMapping("/{id}/etiquetas")
    public ResponseEntity<Collection<Etiqueta>> listarEtiquetasDeLaImagen(@PathVariable Long id){
        Imagen imagen = imagenRepository.findById(id).orElseThrow();
            if(imagen !=null){
                return new ResponseEntity<>(imagen.getEtiquetas(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
    }
    @GetMapping("/dp/{id}")
    public ResponseEntity<Imagen> verDpImagen(@PathVariable Long id){
        Imagen imagen = imagenRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Imagen con Id: "+id+" no encontrada"));
        return ResponseEntity.ok().body(imagen);
    }

    @PutMapping("/dp/{id}")
    public ResponseEntity<Imagen> actualizarDp(@PathVariable Long id, @Valid @RequestBody Imagen dpImagen){
        Imagen imagen = imagenRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Imagen con Id: "+id+" no encontrada"));
        imagen.setDeesPoint(dpImagen.getDeesPoint());
        Imagen imagenActualizada = imagenRepository.save(imagen);
        return ResponseEntity.ok(imagenActualizada);
    }
}



