package com.ufro.Dees.controllers;

import com.ufro.Dees.models.Imagen;
import com.ufro.Dees.models.Reporte;
import com.ufro.Dees.models.Usuario;
import com.ufro.Dees.repositories.ImagenRepository;
import com.ufro.Dees.repositories.ReporteRepository;
import com.ufro.Dees.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/reporte")
public class ReporteController {
    @Autowired
    private ReporteRepository reporteRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    @PostMapping
    public ResponseEntity<Reporte> guardarReporte(@Valid @RequestBody Reporte reporte) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(reporte.getUsuario().getId());
        if(!usuarioOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        reporte.setUsuario(usuarioOptional.get());
        Reporte reporteGuardado =reporteRepository.save(reporte);
        URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(reporteGuardado.getId()).toUri();
        return ResponseEntity.created(ubicacion).body(reporteGuardado);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Reporte> actualizarReporte(@Valid @PathVariable Long id, @Valid @RequestBody Reporte reporte) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(reporte.getUsuario().getId());
        if(!usuarioOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        Optional<Reporte> reporteOptional = reporteRepository.findById(id);
        if(!reporteOptional.isPresent()){
            return ResponseEntity.unprocessableEntity().build();
        }
        reporte.setUsuario(usuarioOptional.get());
        reporte.setId(reporteOptional.get().getId());
        reporteRepository.save(reporte);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Reporte> eliminarReporte(@PathVariable Long id) {
        Optional<Reporte> reporteOptional = reporteRepository.findById(id);
        if (!reporteOptional.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        reporteRepository.delete(reporteOptional.get());
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Reporte> obtenerReporte(@PathVariable Long id) {
        Optional<Reporte> reporteOptional = reporteRepository.findById(id);
        if (!reporteOptional.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }
        return ResponseEntity.ok(reporteOptional.get());
    }
}